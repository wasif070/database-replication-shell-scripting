#!/bin/sh


cd /home/dohatec/;
rm -rf /home/dohatec/mysqlDUMP;
mkdir -p mysqlDUMP;
sudo chmod 777 -R /home/dohatec/mysqlDUMP;

CURRENT_TIME=$(mysql eproc -u root -pPASSWORD -h IP.ADDRESS -se "SELECT NOW()")

mysql -e "select * from t_user_activity where USR_TIMESTAMP < '${CURRENT_TIME}' AND USR_TIMESTAMP >= '${CURRENT_TIME}' - INTERVAL 7 DAY;" -h IP.ADDRESS -uroot -pPASSWORD eproc > /home/dohatec/mysqlDUMP/t_user_activity.csv;

mysql -e "select * from t_ppmo_global_logger where LOG_TIME < '${CURRENT_TIME}' AND LOG_TIME >= '${CURRENT_TIME}' - INTERVAL 7 DAY;" -h IP.ADDRESS -uroot -pPASSWORD eproc > /home/dohatec/mysqlDUMP/t_ppmo_global_logger.csv;

mysql -e "select * from general_log where event_time < '${CURRENT_TIME}' AND event_time >= '${CURRENT_TIME}' - INTERVAL 7 DAY;" -h IP.ADDRESS -uroot -pPASSWORD mysql > /home/dohatec/mysqlDUMP/general_log.csv;

mysql -e "delete from t_user_activity where USR_TIMESTAMP < '${CURRENT_TIME}' AND USR_TIMESTAMP >= '${CURRENT_TIME}' - INTERVAL 7 DAY;" -h IP.ADDRESS -uroot -pPASSWORD eproc;

mysql -e "delete from t_ppmo_global_logger where LOG_TIME < '${CURRENT_TIME}' AND LOG_TIME >= '${CURRENT_TIME}' - INTERVAL 7 DAY;" -h IP.ADDRESS -uroot -pPASSWORD eproc;

mysql -e "delete from general_log where event_time < '${CURRENT_TIME}' AND event_time >= '${CURRENT_TIME}' - INTERVAL 7 DAY;" -h IP.ADDRESS -uroot -pPASSWORD mysql;

