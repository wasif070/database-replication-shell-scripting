#!/bin/sh

cd /home/imtiaz/mysqlDUMP;
rm t_user_activity.csv;
rm t_ppmo_global_logger.csv;
rm general_log.csv;

sshpass -p "PASSWORD" scp USER@IP.ADDRESS:/home/dohatec/mysqlDUMP/t_user_activity.csv /home/imtiaz/mysqlDUMP

sshpass -p "PASSWORD" scp USER@IP.ADDRESS:/home/dohatec/mysqlDUMP/t_ppmo_global_logger.csv /home/imtiaz/mysqlDUMP

sshpass -p "PASSWORD" scp USER@IP.ADDRESS:/home/dohatec/mysqlDUMP/general_log.csv /home/imtiaz/mysqlDUMP

mysql -e "LOAD DATA LOCAL INFILE '/home/imtiaz/mysqlDUMP/t_user_activity.csv' INTO TABLE t_user_activity FIELDS TERMINATED BY '\t' ENCLOSED BY '\"' LINES TERMINATED BY '\n' IGNORE 1 ROWS;" -uadmin -padmin eproc;

mysql -e "LOAD DATA LOCAL INFILE '/home/imtiaz/mysqlDUMP/t_ppmo_global_logger.csv' INTO TABLE t_ppmo_global_logger FIELDS TERMINATED BY '\t' ENCLOSED BY '\"' LINES TERMINATED BY '\n' IGNORE 1 ROWS;" -uadmin -padmin eproc;

mysql -e "LOAD DATA LOCAL INFILE '/home/imtiaz/mysqlDUMP/general_log.csv' INTO TABLE general_log FIELDS TERMINATED BY '\t' ENCLOSED BY '\"' LINES TERMINATED BY '\n' IGNORE 1 ROWS;" -uadmin -padmin mysql;