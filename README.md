# Database Replication Shell Scripting

Replicate data of 3 tables from master database to slave database for a time interval (7 days) and also delete the replicated data from the master DB.

1.	Save the ‘mysqldump.sh‘ file in the master server. It executes 3 select query from now to previous 7 days data. Save the result set in ‘/home/dohatec/mysqlDUMP’ directory. (change the directory) The 3 files name would be- t_user_activity.csv, t_ppmo_global_logger.csv, general_log.csv. After making backup in files it will also remove the data from master tables.

2.	Save the ‘localBash.sh’ file in the slave server. By running this script it will connect to the master server and execute mysqldump.sh script.

3.	In slave, add this localBash.sh file to crontab. Schedule your cron job at the specific time, when you want to make backup from now to previous 7 days data.

4.	Save the ‘localSave.sh’ file in slave server. By running this script it will copy the 3 backup files from the master to local and load the data from files to slave database.

5.	In slave, add this localSave.sh file to crontab. Schedule your cron job at the specific time, when you want to copy the remote files to local and load data to local database.
